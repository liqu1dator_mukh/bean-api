// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

// Since panicking in lib is UB, we should avoid it as much as possible.
#![forbid(clippy::panic)]
#![forbid(clippy::unwrap_used)]
#![forbid(clippy::unwrap_in_result)]
#![forbid(clippy::expect_used)]
#![forbid(clippy::expect_fun_call)]
#![allow(clippy::missing_panics_doc)]

mod config;

type AppId = u32;

use crate::config::CONFIG;
use log::{error, info, warn};
use moka::sync::Cache;
use startup::on_startup;
use std::env;
use std::ffi::c_void;
use std::path::PathBuf;

on_startup! {
    let _ = env_logger::try_init();
    info!("Current dir: \"{}\", current exe: \"{}\"", env::current_dir().unwrap_or_else(|_| PathBuf::from("/dev/null")).display(), env::current_exe().unwrap_or_else(|_| PathBuf::from("/dev/null")).display());
}

#[static_init::dynamic(lazy)]
static CACHE: Cache<AppId, bool> = Cache::new(1024);

#[allow(clippy::missing_safety_doc)]
#[no_mangle]
pub extern "C" fn SteamAPI_ISteamApps_BIsDlcInstalled(_: *mut c_void, app_id: AppId) -> bool {
    match &*CONFIG {
        Ok(config) => {
            if let Some(bool) = config.unlock_all {
                info!("unlock_all is {bool}; returning it to the game");
                return bool;
            } else if let Some(cached_bool) = CACHE.get(&app_id) {
                return cached_bool;
            } else if let Some(app_ids) = &config.unlock_dlc {
                let does_config_have_app_id = app_ids.contains(&app_id);
                info!("Does config have requested app_id ({app_id}): {does_config_have_app_id}");
                CACHE.insert(app_id, does_config_have_app_id);
                return does_config_have_app_id;
            } else {
                warn!("Config is successfully passed, but no unlock_all or unlock_dlc was found. Perhaps a grammar mistake in config file?");
            }
        }
        Err(why) => {
            error!("Error while accessing config: {why}");
        }
    }
    false
}

// hueta kakaya-to
