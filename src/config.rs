// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use crate::AppId;
use log::info;
use serde::Deserialize;
use std::{fs, io};

#[derive(Deserialize, Debug)]
pub struct Config {
    pub unlock_all: Option<bool>,
    pub unlock_dlc: Option<Vec<AppId>>,
}

#[static_init::dynamic(lazy)]
pub static CONFIG: Result<Config, io::Error> = {
    const CONFIG_NAME: &str = "bean-api.toml";
    let file = fs::read(format!("./{CONFIG_NAME}"))?;
    info!("Raw config file: {file:?}");
    let parsed_config = toml::from_slice(&file)?; // wtf? toml::de:Error can be converted to io::Error?
    info!("Parsed config: {parsed_config:?}");
    Ok(parsed_config)
};
