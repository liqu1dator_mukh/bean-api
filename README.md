# BeanAPI

DLC unlocker for Steam (Linux)


DOES NOT WORK, BECAUSE i'm too lazy to fix it, sorry

### Features

* Unlock all DLCs
* Unlock specified DLCs

You still need to get DLC files tho.

## Installation

Download (x32 or x64, depending from game) from [releases page](https://gitlab.com/liqu1dator_mukh/bean-api/-/releases) or
from [CI pipeline page](https://gitlab.com/liqu1dator_mukh/bean-api/-/pipelines)

Put `libbean_api.so` in game directory (you can put it anywhere you want to, but you will have to specify library path
then)

## Usage

Create file named `bean-api.toml` in game directory:

```toml
# if you want to unlock all dlcs:
unlock_all = true
# if you don't want to, then set unlock_all to false or delete entire string above
unlock_dlc = [1, 2, 3, 4, 5] # where is 1, 2, 3, etc... is DLCs app id.
# if unlock_all is true, then unlock_dlc is ignored 
```

You can find app ids on SteamDB, e.g. see [this.](https://steamdb.info/app/480/dlc/)

Run game executable with our injected library using `LB_PRELOAD`: `LB_PRELOAD=/path/to/libbean_api.so ./game`

If you put `libbean_api.so` in game directory, you can run it like that: `LB_PRELOAD=./libbean_api.so ./game`

### Running game from Steam

Add this into [Game Launch Options](https://help.steampowered.com/en/faqs/view/7D01-D2DD-D75E-2955):

```bash
LB_PRELOAD=/path/to/libbean_api.so %command%
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
